
Portfolio

Dans ma formation "Développement web et Mobile" chez simplon, nous avons eu pour premier projet de créer un portfolio.
Ce portfolio doit contenir mon parcours, mes motivations, mes compétences acquises (ou futures) et mes projets.

Sections: Tous les titres ont un effet de surlignement au survol de la souris crée avec un hover et after, et responsive avec bootstrap.

Accueil/A propos
Un barre de navigation fixer en haut avec un position fixed, une photo est mise en fond dans la section avec une description "qui je suis et pourquoi je souhaite devenir développeuse" , ainsi qu'un lien vers mon CV.

Compétences
Cette section contient les différents langage étudiés et acquis durant la formation (vu ou prochainement).

Projets
Cette section presente des cards avec en image les projets, un effet d'ombrage et de survolement à été implémenté.

Outils
Sections qui présente les différents outils utilisés, avec un effet de survol qui met en couleur les logos ainsi qu'un lien vers les sites ou mes comptes.

Footer
Un footer simple avec des liens:
 Le premier ouvre une fenêtre pour m'envoyer un mail
 Le second permet de téléphoner directement à mon numéro
Le troisième ouvre mon CV en PDF disponible au téléchargement


Complément:

Maquette: https://www.figma.com/file/ORkn3tDsrzKrctwi3fBMNq/WireFrame-Portfolio?node-id=0%3A1&t=EqBQRW72JqU5I7EZ-0

Portfolio: https://grobonsamantha.netlify.app/
